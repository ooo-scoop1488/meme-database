from PyQt5.QtTest import QTest
from PyQt5.QtWidgets import QApplication, QLabel, QWidget
from PyQt5.QtCore import Qt, QUrl
from jsonLib import Picture, FileReader, Tag
from os import rename, remove
import sys
import unittest     
from UI.window import *
from UI.imageLabel import ImageLabel, TagLabel

app = QApplication(sys.argv)
fr = FileReader()

def reset():
    try:
        remove(fr._currentDir + "/data")
    except OSError:
        pass

class TestGui(unittest.TestCase):

    def setUp(self):
        self.form = Window("TEST")
        self.text = "test"

    def test_enterTagsTab(self):
        self.form.pictureAddTab.textTagsField.setText("#SFU")
        file = ('C:/Users/я/Desktop/основы программирования/pJfVj7bTncw.jpg', 'All Files (*)')
        self.form.pictureAddTab.drops.links.append(file[0])
        self.form.pictureAddTab.drops.refreshPicture()
        QTest.mouseClick(self.form.pictureAddTab.acceptBtn, Qt.LeftButton)
        tag = self.form.labels["all"][-1].tags[-1]
        tag.name = "SFU"
        self.assertEqual(self.form.labels["all"][-1].texttags,["SFU"])
        self.form.delImage(label=self.form.labels["all"][-1])
        self.form.fileReader.deleteTag(tag)
        

    def test_tagClicked(self):
        tag = TagLabel(parent = None, text = self.text, showTag = self.form.addSearchTag)
        QTest.mousePress(tag, Qt.LeftButton)
        self.assertEqual(self.form.ui.searchLine.text(), "test")
        

    def test_addImgToLayout(self):
        file = ('C:/Users/я/Desktop/основы программирования/pJfVj7bTncw.jpg', 'All Files (*)')
        self.form.pictureAddTab.drops.links.append(file[0])
        self.form.pictureAddTab.drops.refreshPicture()
        startLen = len(self.form.labels["all"])
        QTest.mouseClick(self.form.pictureAddTab.acceptBtn, Qt.LeftButton)
        pic = self.form.labels["all"][-1]
        self.assertEqual(len(self.form.labels["all"]),startLen + 1)
        self.form.delImage(label=self.form.labels["all"][-1])
        self.form.fileReader.deletePicture(pic)

    def test_dropImg(self):
        url =QUrl('file:///C:/Users/я/Desktop/основы программирования/iBgXJ1CPW_g.jpg')
        self.form.pictureAddTab.drops.links.append(str(url.toLocalFile()))
        self.form.pictureAddTab.drops.refreshPicture()
        QTest.mouseClick(self.form.pictureAddTab.acceptBtn, Qt.LeftButton)
        pic = self.form.labels["all"][-1]
        self.assertEqual(len(self.form.labels["all"]), 1)
        self.form.delImage(label=self.form.labels["all"][-1])
        self.form.fileReader.deletePicture(pic)

    def test_search(self):
        self.form.pictureAddTab.textTagsField.setText("#SFU")
        file = ('C:/Users/я/Desktop/основы программирования/pJfVj7bTncw.jpg', 'All Files (*)')
        self.form.pictureAddTab.drops.links.append(file[0])
        self.form.pictureAddTab.drops.refreshPicture()
        QTest.mouseClick(self.form.pictureAddTab.acceptBtn, Qt.LeftButton)
        text = self.form.labels["all"][-1].texttags[0]
        self.form.ui.searchLine.insert(text)
        QTest.mouseClick(self.form.ui.searchButton, Qt.LeftButton)
        self.assertEqual(len(self.form.labels["all"]),0)

        


"""
    def test_layoutTagsTab(self):
        self.form.pictureAddTab.description.setText("#SFU#IKIT")
        self.form.pictureAddTab.addTags()
        self.form.file = ('C:/Users/я/Desktop/основы программирования/pJfVj7bTncw.jpg', 'All Files (*)')
        self.form.inChoise = True
        QTest.mouseClick(self.form.pictureAddTab.acceptBtn, Qt.LeftButton)
        self.assertEqual(self.form.layoutTagsTab.)
  """

    









        
    






if __name__ == "__main__":
    unittest.main()

    



