from json import dump, load
from os import mkdir, remove, getcwd
from os.path import sep, exists
from random import randint
from shutil import copyfile, rmtree
from threading import RLock, Thread


def startThread(func):
    def wrapper(self):
        thread = Thread(target=func, args=(self,))
        thread.start()
    return wrapper


class Tag:
    """
    класс - тип данных для хранения информации о тэгах внутри программы
    """

    def __init__(self, name: str, color, synonyms: list):
        """
        name = str,  название и текст тэга
        color = ???, цвет тэга
        synonyms = list из str, синонимы
        """
        self.name = name
        self.color = color
        self.synonyms = synonyms

    def dictTag(self):
        """
        возвращает представление тэга в виде словаря
        """
        return {self.name: {"color": self.color, "synonyms": self.synonyms}}


class Picture:
    """
    класс - тип данных для хранения информации о картике внутри программы
    """

    def __init__(self, path: str, width: int, height: int, ID: int, description: str, tagsID, user: str):
        """
        path = str, путь к файлу изображения
        width = int, ширина изображения
        height = int, высота изображения
        ID = int, идентификационный номер картинки
        description = str, описание картинки
        tagsID = list из str, все тэги картинки
        user = str, пользователь, создавший картинку
        """
        self.ID = str(ID)
        self.path = path
        self.width = width
        self.height = height
        self.description = description
        self.tagsID = tagsID
        self.user = user

    def dictPic(self):
        return {str(self.ID): {"path": self.path, "width": self.width, "height": self.height,
                               "description": self.description, "tagsID": self.tagsID, "username": self.user}}


class MetaSingleton(type):
    """
    Мета-класс, реализующий singleton архетип
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class FileReader(metaclass=MetaSingleton):
    """
    Класс, отвечающий за работу с базой данных.

    Информация записывается в json-файлы.
    """
    # это всё вам не надо
    __dataDir = "/data"
    __additionalFile = __dataDir + "/additionalFile.json"
    __mainFile = __dataDir + "/mainFile.json"
    __tagFile = __dataDir + "/tagFile.json"
    __picAutoIncrementConst = "picAutoIncrement"
    __tagAutoIncrementConst = "tagAutoIncrement"
    __pathConst = "path"
    __widthConst = "width"
    __heightConst = "height"
    __randomUserNameConst = "Random user №"
    __usernameConst = "username"
    __deletedPicIndexesConst = "deletedPicIndexes"
    __deletedTagIndexesConst = "deletedTagIndexes"
    __tagsIdConst = "tagsID"
    __descriptionConst = "description"
    __ignoredUsersConst = "ignoredUsers"
    __synonymsConst = "synonyms"
    __languageConst = "language"

    def __init__(self):
        self.lock = RLock()
        self._currentDir = getcwd().replace(sep, '/')
        self.__currentPictureId = -1
        self.__currentTagId = -1
        self.__additionalData = {}
        self._tagData = {}
        self.__tagList = list()
        self.__picList = list()
        self._data = {}

        """with ThreadPoolExecutor(max_workers=1) as thread:
            thread.submit(self.__loadData)"""
        self.__loadData()

    def createPicture(self, path: str, width: int, height: int, description=""):
        """
        И создаёт (возвращает) экземпляр картинки, и сохраняет её в файл.

        Для создания Picture использовать ТОЛЬКО этот метод! Констуктор Picture не сохранит ее в базу

        Если картинки удалялись, то у текущей картинки будет освободившийся индекс.

        description = str, описание
        path = str, путь картинки
        width = int, ширина картинки (любая удобная еденица измерения)
        height = int высота картинки (любая удобная еденица измерения)
        """
        oldPath = path
        path = path[path.rfind('/'):]
        user = self.__additionalData[self.__usernameConst]
        try:
            copyfile(oldPath, self._currentDir + self.__dataDir + path)
        except OSError:
            return 0

        length = len(self.__additionalData[self.__deletedPicIndexesConst])
        if length >= 1:
            picture = Picture(path, width, height,
                              self.__additionalData[self.__deletedPicIndexesConst][length - 1], description, [], user)
            del self.__additionalData[self.__deletedPicIndexesConst][length - 1]
        else:
            self.__additionalData[self.__picAutoIncrementConst] += 1
            picture = Picture(path, width, height, self.__additionalData[self.__picAutoIncrementConst],
                              description, [], user)

        self._data.update(picture.dictPic())
        self.__picList.append(picture.ID)
        self.__dumpData()

        picture.path = self._currentDir + self.__dataDir + picture.path
        return picture

    def deletePicture(self, picture: Picture):
        """
        Удаляет картинку, которую закинули в параметр из файла и из времменного хранилища.

        Замечание: экземпляр картинки, кторый закинули остаётся.
        Удалите его оттуда, откуда вызывали deletePicture с помощью del.

        Если такого элемента нет в базе, то ничего не происходит.

        picture = Picture, картинка которую нужно удалить
        """
        try:
            deleted = self.__additionalData[self.__deletedPicIndexesConst]
            deleted = list(deleted)

            del self._data[picture.ID]
            self.__picList.remove(picture.ID)
            deleted.append(picture.ID)

            if exists(picture.path):
                remove(picture.path)

            for i in range(0, int(self.__additionalData[self.__picAutoIncrementConst]) + 1):
                if str(i) in self._data:

                    if self._data[str(i)][self.__pathConst] == picture.path[picture.path.rfind("/"):]:
                        del self._data[str(i)]
                        self.__picList.remove(str(i))
                        deleted.append(str(i))

            self.__additionalData[self.__deletedPicIndexesConst] = deleted
            self.__dumpData()
        except KeyError:
            pass
        except AttributeError:
            pass

    def loadNextPicture(self, fromStart=False, request=""):
        """
        Метод загрузки каринок с запросом и без

        Метот возвращает кортеж, состоящий из экземпляра Picture и информации о загрузке.
        В случае неудачной загрузки возвращается None.
        0 - успешная загрузка
        1 - достигнут конец списка картинок
        2 - картинка не проходит по запросу
        3 - картинка от одного из заблокированных пользователей

        fromStart = bool, Начать загрузку сначала
        request = str, запрос с фильтром, в виде строки. поддерживается [и, или, не] как ['&', ' ', '-']
        """
        if fromStart:
            self.__currentPictureId = -1
        self.__currentPictureId += 1
        if self.__currentPictureId >= len(self.__picList):
            return None, 1

        ID = self.__picList[self.__currentPictureId]

        if not self._data[ID][self.__usernameConst] in self.__additionalData[self.__ignoredUsersConst]:
            if request == "":
                path = self._data[ID][self.__pathConst]
                width = self._data[ID][self.__widthConst]
                height = self._data[ID][self.__heightConst]
                tagsID = self._data[ID][self.__tagsIdConst]
                description = self._data[ID][self.__descriptionConst]
                user = self._data[ID][self.__usernameConst]
                return Picture(self._currentDir + self.__dataDir + path, width, height, ID, description, tagsID, user), 0
            else:
                currTags = list(self._data[ID][self.__tagsIdConst])

                title = self._data[ID][self.__pathConst][1:self._data[ID][self.__pathConst].rfind(".")]

                orClusters = str.split(request, " ")
                for orCluster in orClusters:
                    andWords = orCluster.split("&")
                    flag = True
                    if len(orCluster) > 0:
                        for andWord in andWords:
                            if flag is False:
                                break
                            if len(andWord) > 0:
                                if andWord[0] == "-":
                                    andWord = andWord[1:]
                                    if andWord in currTags or andWord == title \
                                            or andWord in self._data[ID][self.__descriptionConst]:
                                        flag = False
                                    else:
                                        flag = self.__innerLoop(andWord, currTags, positive=False)
                                else:
                                    if andWord not in currTags and andWord != title \
                                            and andWord not in self._data[ID][self.__descriptionConst].split():
                                        flag = self.__innerLoop(andWord, currTags)
                    if flag is True:
                        break

                if flag:
                    path = self._data[ID][self.__pathConst]
                    width = self._data[ID][self.__widthConst]
                    height = self._data[ID][self.__heightConst]
                    tagsID = self._data[ID][self.__tagsIdConst]
                    description = self._data[ID][self.__descriptionConst]
                    user = self.__additionalData[self.__usernameConst]
                    return Picture(self._currentDir + self.__dataDir + path,
                                   width, height, ID, description, tagsID, user), 0
                else:
                    return None, 2
        else:
            return None, 3

    def __innerLoop(self, andWord, currTags, positive=True):
        """
        вспомогательный метод для поиска в oadNextPic
        """
        if len(currTags) > 0:
            for tag in currTags:
                if andWord in self._tagData[tag][self.__synonymsConst]:
                    return True * positive + False * (not positive)
            return False * positive + True * (not positive)
        else:
            return False

    def createTag(self, name: str, color=None, synonyms=None):
        """
        работает аналогично createPicture, только для тэгов

        возвращает None если такой тэг уже существует

        name = str, сам тэг
        color = ???, цвет
        synonyms = list из str, лист синонимов
        """
        if synonyms is None:
            synonyms = []
        if color is None:
            color = [[230, 230, 230], [0, 0, 0]]
        if name in self._tagData:
            return Tag(name, color, synonyms)
        tag = Tag(name, color, synonyms)
        self._tagData.update(tag.dictTag())
        self.__dumpData()
        self.__tagList.append(tag)

        return tag

    def addTagToPic(self, tag: Tag, picture: Picture):
        """
        добавляет тэг к картинке

        tag = Tag
        picture = Pictue, картинка, к которой нужно добавить тэг
        """
        self._data[picture.ID][self.__tagsIdConst].append(tag.name)
        self.__dumpData()

    def addSomeTagsToPic(self, tagList: list, picture: Picture):
        """
        добавляет целый список тэгов к картинке, а остальное аналогично
        """
        for tag in tagList:
            picture.tagsID.append(tag)
        self.__dumpData()

    def deleteTag(self, tag: Tag):
        """
        работает аналогично deletePic
        tag = Tag, тэг для удаления
        """
        deleted = self.__additionalData[self.__deletedTagIndexesConst]
        deleted = list(deleted)
        deleted.append(tag.name)
        self.__additionalData[self.__deletedTagIndexesConst] = deleted

        del self._tagData[tag.name]

        for picture in self._data:
            if tag.name in self._data[picture][self.__tagsIdConst]:
                try:
                    del self._data[picture][self.__tagsIdConst][self._data[picture][self.__tagsIdConst].index(tag.name)]
                except ValueError:
                    pass
        self.__dumpData()

    def deleteTagName(self, name: str):
        """
        удаляет тэг по имени, а не по экземпляру тэг

        TODO: объединить в один
        """
        deleted = self.__additionalData[self.__deletedTagIndexesConst]
        deleted = list(deleted)
        deleted.append(name)
        self.__additionalData[self.__deletedTagIndexesConst] = deleted

        del self._tagData[name]

        for picture in self._data:
            if name in self._data[picture]["tagsID"]:
                try:
                    del self._data[picture]["tagsID"][self._data[picture]["tagsID"].index(name)]
                except ValueError:
                    pass
        self.__dumpData()

    def deleteTagFromPic(self, tag: Tag, pic: Picture):
        """
        удалять тэг из картинки, но не из базы тэгов
        tag
        pic
        """
        del self._data[pic.ID][self.__tagsIdConst][self._data[pic.ID][self.__tagsIdConst].index(tag.name)]
        self.__dumpData()

    def loadNextTag(self, fromStart=False):
        """
        загружает (возвращает) по 1 тэгу из базы
        fromStart = bool, если надо начать сначала
        """
        with self.lock:
            if fromStart:
                self.__currentTagId = -1

            self.__currentTagId += 1

            if self.__currentTagId >= len(self.__tagList):
                return None
            tag = self.__tagList[self.__currentTagId]
            return tag

    def reSortPics(self):
        """
        пересортирует картинки
        """
        self.__picList.sort(key=lambda x: (self._data[x][self.__pathConst]))

    def reSortTags(self):
        """"
        пересортирует тэги
        """
        self.__tagList.sort(key=lambda x: (x.name, x.color))

    def addIgnore(self, user: str):
        """
        добавляет пользователя в список исключений
        user = str, имя пользователя
        """
        if len(self.__additionalData[self.__ignoredUsersConst]) == 0:
            self.__additionalData[self.__ignoredUsersConst] = list()
        self.__additionalData[self.__ignoredUsersConst].append(user)
        self.__dumpData()

    def delIgnore(self, user: str):
        """
        убирает пользователя из списка исключений
        user = str, имя пользователя
        """
        if user in self.__additionalData[self.__ignoredUsersConst]:
            del self.__additionalData[self.__ignoredUsersConst][
                self.__additionalData[self.__ignoredUsersConst].index(user)]
        self.__dumpData()

    def clearIgnore(self):
        """
        полностью очищает список исключений
        """
        self.__additionalData[self.__ignoredUsersConst] = []

    def changeLanguage(self, language: str):
        """
        изменяет язык
        language = str, язык
        """
        self.__additionalData[self.__languageConst] = language
        self.__dumpData()

    def getSettings(self):
        """
        возвращает текущее значение настроек в виде словаря
        """
        return {"username": self.__additionalData[self.__usernameConst], \
                "ignored": self.__additionalData[self.__ignoredUsersConst], \
                "language": self.__additionalData[self.__languageConst], }

    def setDescription(self, pic: Picture, description: str):
        self._data[pic.ID][self.__descriptionConst] = description
        self.__dumpData()

    def setPath(self, pic: Picture, path: str):
        print("!!!!!!!!!!!!!!!!!1", path)
        self._data[pic.ID][self.__pathConst] = path
        self.__dumpData()

    def setTags(self, pic: Picture, tags: list):
        self._data[pic.ID][self.__tagsIdConst] = tags
        for tag in tags:
            self.createTag(tag)
        self.__dumpData()

    def __makeFiles(self):
        """
        внутренний метод, который создаёт пустые файлы базы данных
        """
        try:
            mkdir(self._currentDir + self.__dataDir)
        except OSError:
            pass
        try:
            remove(self._currentDir + self.__mainFile)
        except OSError:
            pass
        try:
            remove(self._currentDir + self.__tagFile)
        except OSError:
            pass
        try:
            remove(self._currentDir + self.__additionalFile)
        except OSError:
            pass

        username = self.__randomUserNameConst + str(randint(1000, 9999))
        initialPicAutoIncrement = -1
        initialTagAutoIncrement = -1
        initialDeletedIndexesArray = list()
        self.__additionalData = {self.__usernameConst: username, self.__picAutoIncrementConst: initialPicAutoIncrement,
                                 self.__tagAutoIncrementConst: initialTagAutoIncrement,
                                 self.__deletedPicIndexesConst: initialDeletedIndexesArray,
                                 self.__deletedTagIndexesConst: [], "ignoredUsers": [], "language": "English"}
        self.__picList = list()

        with open(self._currentDir + self.__additionalFile, "w+") as addiFile:
            dump(self.__additionalData, addiFile)

        self._data = {}
        with open(self._currentDir + self.__mainFile, "w+") as mainFile:
            dump(self._data, mainFile)

        self._tagData = {}
        with open(self._currentDir + self.__tagFile, "w+") as tagFile:
            dump(self._data, tagFile)

    @startThread
    def __loadData(self):
        """
        внутренни метод, который загружает данные из файлов
        """
        with self.lock:
            try:
                with open(self._currentDir + self.__additionalFile, "r") as addiFile:
                    self.__additionalData = load(addiFile)
            except OSError:
                self.__makeFiles()

            try:
                with open(self._currentDir + self.__mainFile, "r") as mainFile:
                    self._data = load(mainFile)
            except OSError:
                self.__makeFiles()

            try:
                with open(self._currentDir + self.__tagFile, "r") as tagFile:
                    self._tagData = load(tagFile)
            except OSError:
                self.__makeFiles()

            for tag in self._tagData:
                self.__tagList.append(Tag(tag, self._tagData[tag]["color"], self._tagData[tag]["synonyms"]))
            for pic in self._data:
                self.__picList.append(pic)

            self.__tagList.sort(key=lambda x: x.name)
            self.__picList.sort(key=lambda x: (self._data[x]["path"]))

    @startThread
    def __dumpData(self):
        """
        внутренний метод, который загружает данные в файлы
        """
        with self.lock:
            try:
                with open(self._currentDir + self.__mainFile, "w") as mainFile:
                    dump(self._data, mainFile)
            except OSError:
                self.__makeFiles()
            try:
                with open(self._currentDir + self.__additionalFile, "w") as addiFile:
                    dump(self.__additionalData, addiFile)
            except OSError:
                self.__makeFiles()
            try:
                with open(self._currentDir + self.__tagFile, "w") as tagFile:
                    dump(self._tagData, tagFile)
            except OSError:
                self.__makeFiles()

    def combineData(self):
        """
        метод для объединения данных при импорте базы.
        совмещает дату в папке tempData с датой в папке data и удаляет tempData
        """
        tempPath = self._currentDir + "/tempData"
        if not exists(tempPath):
            return 0
        if not (exists(tempPath + "/additionalFile.json") and exists(tempPath + "/mainFile.json")
                and exists(tempPath + "/tagFile.json")):
            return -1

        with open(tempPath + "/additionalFile.json", "r") as exA:
            tempAdditionalData = load(exA)

        self.__additionalData[self.__picAutoIncrementConst] += tempAdditionalData[self.__picAutoIncrementConst]
        self.__additionalData[self.__deletedPicIndexesConst] += tempAdditionalData[self.__deletedPicIndexesConst]
        self.__additionalData[self.__deletedTagIndexesConst] += tempAdditionalData[self.__deletedTagIndexesConst]

        with open(tempPath + "/mainFile.json", "r") as exM:
            dic = load(exM)
            self._data.update(dic)
            for pic in dic:
                self.__picList.append(pic)
        with open(tempPath + "/tagFile.json", "r") as exT:
            self._tagData.update(load(exT))
        rmtree(self._currentDir + "/tempData")
        self.__dumpData()
        return 1

    def setDescription(self, pic: Picture, description: str):
        self._data[pic.ID][self.__descriptionConst] = description
        self.__dumpData()

    def setPath(self, pic: Picture, path: str):
        self._data[pic.ID][self.__pathConst] = path[path.rfind("/"):]
        self.__dumpData()

    def setTags(self, pic: Picture, tags: list):
        self._data[pic.ID][self.__tagsIdConst] = tags
        self.__dumpData()

    @startThread
    def changeUser(self, newUser: str):
        for pic in self._data:
            if pic[self.__usernameConst] == self.__additionalData[self.__usernameConst]:
                pic[self.__usernameConst] = newUser

        self.__additionalData[self.__usernameConst] = newUser



if __name__ == "__main__":
    FileReader().combineData()
