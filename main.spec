# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['C:\\memdb\\meme-database'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
a.datas += [('exportbutton.png', 'C:\\memdb\\meme-database\\UI\\exportbutton.png', 'DATA'),('home-button.png', 'C:\\memdb\\meme-database\\UI\\home-button.png', 'DATA'),('searchicon.svg', 'C:\\memdb\\meme-database\\UI\\searchicon.svg', 'DATA'),('settingsicon.png', 'C:\\memdb\\meme-database\\UI\\settingsicon.png', 'DATA'),('Picture1.jpg', 'C:\\memdb\\meme-database\\UI\\Picture1.jpg', 'DATA'),('Picture2.jpg', 'C:\\memdb\\meme-database\\UI\\Picture2.jpg', 'DATA'),('Picture3.jpg', 'C:\\memdb\\meme-database\\UI\\Picture3.jpg', 'DATA'),('Picture4.jpg', 'C:\\memdb\\meme-database\\UI\\Picture4.jpg', 'DATA'),('Picture5.jpg', 'C:\\memdb\\meme-database\\UI\\Picture5.jpg', 'DATA'),('Picture6.jpg', 'C:\\memdb\\meme-database\\UI\\Picture6.jpg', 'DATA'),('Picture7.jpg', 'C:\\memdb\\meme-database\\UI\\Picture7.jpg', 'DATA'),('Picture8.jpg', 'C:\\memdb\\meme-database\\UI\\Picture8.jpg', 'DATA'),('Picture9.jpg', 'C:\\memdb\\meme-database\\UI\\Picture9.jpg', 'DATA'),('Picture10.jpg', 'C:\\memdb\\meme-database\\UI\\Picture10.jpg', 'DATA'),('Picture11.jpg', 'C:\\memdb\\meme-database\\UI\\Picture11.jpg', 'DATA'),('Picture12.jpg', 'C:\\memdb\\meme-database\\UI\\Picture12.jpg', 'DATA'),('Picture13.jpg', 'C:\\memdb\\meme-database\\UI\\Picture13.jpg', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='Meme-Database',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
