from PyQt5.QtWidgets import QApplication
from sys import argv, exit

# from imagefirst import MyWindow
from UI.window import Window


def main():
    app = QApplication(argv)
    window = Window("True")
    window.setWindowTitle("MEMELANDS")
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
