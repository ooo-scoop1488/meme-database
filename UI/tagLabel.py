from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QFrame, QHBoxLayout, QLabel, QPushButton

from jsonLib import FileReader
VTAG = 30


class TagLabel(QFrame):
    def __init__(self, parent, text, showTag):
        super(TagLabel, self).__init__(parent)
        self.layout = QHBoxLayout(self)
        self.setContentsMargins(0, 0, 0, 0)
        self.layout.setAlignment(Qt.AlignTop | Qt.AlignHCenter)
        self.label = QLabel()
        self.label.setText(f" {text} ")
        self.text = text
        self.label.setFixedHeight(VTAG)
        self.setFixedHeight(VTAG)
        self.setStyleSheet("background-color: rgb(210, 210, 210); border-radius: 5px; border: 0px solid rgb(80, 80, 80);")
        font = QFont()
        font.setBold(True)
        self.label.setFont(font)
        self.label.setStyleSheet("color: rgb(0, 0, 0); font: 15pt \"Century Gothic\"; border: 0px; text-align: center | right;")
        self.label.setAlignment(Qt.AlignTop)
        self.showTag = showTag
        self.mousePressEvent = self.pingTag
        self.layout.addWidget(self.label, Qt.AlignCenter)
        self.parent = parent
        if str(type(parent)) == "<class 'UI.window.Window'>":
            self.delBtn = QPushButton()
            self.delBtn.setFixedSize(15, 15)
            self.delBtn.setText('x')
            self.delBtn.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                                      "font: 25 10pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                                      "border: 1px solid rgb(80, 80, 80); text-align: bottom;}\n"
                                      "QPushButton:pressed { background-color: rgb(180,180,180);}")
            self.delBtn.clicked.connect(self.delTag)
            self.layout.addWidget(self.delBtn, Qt.AlignRight)

    def pingTag(self, event):
        self.showTag(self.text)

    def delTag(self):
        FileReader().deleteTagName(self.text)
        self.deleteLater()
        for image in self.parent.labels[self.parent.currReq]:
            for tag in image.tags:
                if self.text == tag.text:
                    image.tags.remove(tag)
                    tag.deleteLater()
