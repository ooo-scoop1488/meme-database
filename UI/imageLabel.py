from functools import wraps
from os import path
from sys import exc_info

import PIL
from PIL import Image
from PyQt5.QtCore import Qt, QSize, QThread
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtWidgets import QFrame, QVBoxLayout, QLabel, QSpacerItem, QSizePolicy, QHBoxLayout, QPushButton

from jsonLib import Picture, FileReader

MEME_HEIGHT = 150  # Fixed height of images
WSIZE = 250
VSIZE = 250
VTAG = 30
SYMBOLS = 45




class ImageLabel(QFrame):
    """
        Фрейм с мемом и тэгами, который добавляется на
        основное окно.
    """
    def __init__(self, parent, deletion, pic: Picture, showTag, opening):
        super(ImageLabel, self).__init__(parent)
        """
            parent = class Window, объект, которому принадлежит фрейм с мемом
            deletion = func, функция, которая удаляет объект снаружи
            pic = Picture, объект json класса с информацие о меме
            texttags = list, список тэгов в виде списка строк
            showTag = ..?
            opening = func, функция, открывающая картинку на "весь экран" снаружи
        """
        # Saving main properties
        self.pic = pic
        self.imageV = 0
        self.width, self.height = WSIZE, VSIZE
        # Bleeping a layout onto a Frame (self)
        self.layout = QVBoxLayout(self)
        self.layout.setAlignment(Qt.AlignHCenter)

        self.setFixedSize(QSize(WSIZE, VSIZE+50))

        self.pictureAlign = QHBoxLayout()
        self.pictureAlign.setAlignment(Qt.AlignCenter)
        self.delete = deletion

        # Creating a label aka actual image. Sizing, scaling and stuff
        self.image = QLabel()
        pix = QPixmap(self.pic.path)
        self.image.setPixmap(pix)
        self.image.setScaledContents(True)
        try:
            self.image.setFixedSize(QSize(*self.resize(pix.width(), pix.height())))
        except ZeroDivisionError:
            print(f">> Error: no such file {self.pic.path}")
            self.toDelete()
        self.image.setText("")
        print(self.imageV)
        self.space()

        # Bleeping image onto the frame
        self.pictureAlign.addWidget(self.image)

        self.layout.addLayout(self.pictureAlign)
        self.texttags = pic.tagsID
        self.tagsCount = len(self.texttags)
        self.tags = []
        self.showTag = showTag

        self.tagFrame = QFrame()
        self.tagFrame.setFixedSize(QSize(WSIZE - 8, 50))
        self.tagFrame.setStyleSheet("background-color: rgb(32, 32, 32); border: 2px solid rgb(255,20,147);")

        self.tagsLayout = QHBoxLayout(self.tagFrame)
        self.tagsLayout.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        self.layout.addWidget(self.tagFrame)

        self.displayTags()

        # Reassigning some of the functions
        self.openPic = opening
        self.mousePressEvent = self.clicked

        # Showing on screen
        self.show()

    def clicked(self, event):
        """
            Функция, которая выполняется при нажатии курсором на картинку.
            Открывает мем в большом окне.

            event = list?, qt сам передает. Описывает ивент.
        """
        self.openPic(self)

    def toDelete(self):
        """
            Запускает удаление картинки.
        """
        self.delete(self)

    def resize(self, originWidth, originHeight):
        """
            Пересчитывает размер картинки, чтобы уменьшить её
            до необходимого, подходящего под окно.

            Возвращает кортеж с новыми размерами.

            originWidth = int,  изначальная ширина картинки
            originHeight = int, изначальная высота картинки
        """
        if originWidth >= originHeight:
            self.imageV = round(originHeight / (originWidth / WSIZE))
            return WSIZE, round(originHeight / (originWidth / WSIZE))
        else:
            if originHeight > VSIZE:
                self.imageV = VSIZE
            else:
                self.imageV = originHeight
            return round(originWidth / (originHeight / VSIZE)), VSIZE

    def space(self):
        """
            Добавляет расстояние между объектами в основной фрейм
        """
        spacer = QSpacerItem(20, 10, QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.layout.addItem(spacer)

    def displayTags(self):
        """
            Преобразует тэги из текстового вида в набор
            фреймов, определенное количество которых в
            последствии отображаются на основном фрейме.
        """
        self.texttags = self.pic.tagsID
        self.tagsCount = len(self.texttags)
        if self.texttags:
            length = 0
            for tag in self.texttags:
                length += len(tag)
                self.tags.append(TagLabel(self, tag, self.showTag))
                if length < SYMBOLS:
                    length += 8
                    self.tagsCount -= 1
            if self.tagsCount > 0:
                self.dull = TagLabel(self, f'+{self.tagsCount}', showTag=None)
                self.tagsCount+1
                self.dull.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)

            self.redisplaytags()

    def hideTags(self):
        """
            Скрывает все тэги с основного фрейма.
        """
        for i in reversed(range(self.tagsLayout.count())):
            widgetToRemove = self.tagsLayout.itemAt(i).widget()
            if widgetToRemove:
                self.tagsLayout.removeWidget(widgetToRemove)
                widgetToRemove.setParent(None)
        
    def wipeTheTags(self):
        self.hideTags()
        for tag in self.tags:
            tag.deleteLater()
            # tag.delTag()
        self.tags = []

    def redisplaytags(self):
        """
            Снова отображает тэги на основном фрейме.
        """
        for i in range(len(self.texttags) - self.tagsCount):
            self.tagsLayout.addWidget(self.tags[i])
        for i in range(len(self.texttags) - self.tagsCount, len(self.texttags)):
            self.tags[i].setParent(None)
        if self.tagsCount > 0:
            self.tagsLayout.addWidget(self.dull)


class TagLabel(QFrame):
    """
        Класс, который отвечает за создание фрейма с
        написанным на нем тэгом.
    """
    def __init__(self, parent, text, showTag):
        """
            parent = ImageLabel,  объект мема, которому принадлежит тэг
            text = str, текст, который необходимо добавить на тэг.
            showTag = ?, ???
        """
        super(TagLabel, self).__init__(parent)
        self.layout = QHBoxLayout(self)
        self.setContentsMargins(0, 0, 0, 0)
        self.layout.setAlignment(Qt.AlignTop | Qt.AlignHCenter)
        self.label = QLabel()
        self.label.setText(f" {text} ")
        self.text = text
        # if not (str(showTag) == '0x77745'):
        self.label.setFixedHeight(VTAG)
        self.setFixedHeight(VTAG)
        self.setStyleSheet(
            "background-color: rgb(210, 210, 210); border-radius: 5px; border: 0px solid rgb(80, 80, 80);")
        font = QFont()
        font.setBold(True)
        self.label.setFont(font)
        # self.label.setStyleSheet(
        #     "color: rgb(0, 0, 0); font: 15pt \"Century Gothic\"; border: 0px; text-align: center | right;")
        # self.label.setStyleSheet("color: rgb(0, 0, 0); font: 8pt \"Century Gothic\"; border: 0px; text-align: center;")
        self.label.setAlignment(Qt.AlignTop)
        self.showTag = showTag
        self.mousePressEvent = self.pingTag
        self.layout.addWidget(self.label, Qt.AlignCenter)
        self.parent = parent
        if (str(type(parent)) == "<class 'UI.window.Window'>") or (str(showTag) == '0x77745'):
            self.delBtn = QPushButton()
            self.delBtn.setFixedSize(15, 15)
            self.delBtn.setText('x')
            self.delBtn.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                                      "font: 25 10pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                                      "border: 1px solid rgb(80, 80, 80); text-align: bottom;}\n"
                                      "QPushButton:pressed { background-color: rgb(180,180,180);}")
            if (str(showTag) == '0x77745'):
                self.delBtn.clicked.connect(self.softDel)
            else:
                self.delBtn.clicked.connect(self.delTag)
            self.layout.addWidget(self.delBtn, Qt.AlignRight)

    def pingTag(self, event):
        """
            Я честно хз что это такое, я не помню чтобы это писал.

            event = ?, qt сам передает, описывает ивент.
        """
        self.showTag(self.text)

    def softDel(self):
        """
            Супер секретная функция, которая помогает временным
            тэгам удаляться. шшшш, не трогай.
        """
        self.bye = True
        self.deleteit()

    def delTag(self):
        """
            Функция удаляет тэг из списков и из
            основного фрейма. насовсем.
        """
        FileReader().deleteTagName(self.text)
        self.deleteLater()
        for image in self.parent.labels[self.parent.currReq]:
            for tag in image.tags:
                if self.text == tag.text:
                    image.tags.remove(tag)
                    tag.deleteLater()
