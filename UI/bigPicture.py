import subprocess

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QGridLayout, QHBoxLayout, QLabel, QVBoxLayout, \
    QSizePolicy, QPushButton, QFrame,  QTextEdit, QScrollArea, QSpacerItem, QLineEdit, QApplication
from PyQt5.QtGui import QPixmap
from UI.imageLabel import TagLabel

SPECIAL_SYMBOLS = ('&', '#', '-')


class mFrame(QFrame):
    """
        Класс, который создает большое окно с картинкой. тэгами и т.д.
    """
    def __init__(self, parent, horizontalSize, verticalSize, coordinates, supahclose, reader):
        """
            parent = Window, объекст основного окна
            horizontalSize = int, размеры основного окна по горизонтали
            verticalSize = int, размеры основного окна по вертикали
            coordinates = tuple of ints, обработанные координаты для фрейма
            supahclose = func, функция извне, которая закрывает все табы
            reader = FileReader, соединение с базой данных
        """
        super(mFrame, self).__init__(parent)

        # Initialization of the Frame
        self.reader = reader
        self.editMode = False
        self.resize(horizontalSize - 200, verticalSize - 200) 
        self.move(*coordinates)
        self.setVisible(False)
        self.shown = False
        self.setStyleSheet("QFrame { background-color: rgb(234,234,234); border: 2px solid rgb(80, 80, 80); }")
        self.layout = QGridLayout(self)
        self.picture = None
        # self.layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.layout)

        self.closeBtn = QPushButton()
        self.closeBtn.setFixedSize(35, 35)
        self.closeBtn.setText('x')
        self.closeBtn.setStyleSheet("QPushButton { background-color: rgb(40, 40, 40); "
                            "font: 25 24pt \"Tahoma\"; border-radius: 1px; color: rgb(220, 220, 220); "
                            "border: 2px solid rgb(40, 40, 40); text-align: bottom}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180); }")
        self.closeSequence = supahclose
        self.closeBtn.clicked.connect(self.closeSequence)
        self.layout.addWidget(self.closeBtn, 0, 3, 1, 1)
        self.image = QLabel()
        self.image.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.image.setStyleSheet("border: 0px")
        # self.image.setMinimumWidth(100)
        self.layout.addWidget(self.image, 0,0,3,2)


        self.infoFrame = QFrame()
        self.infoFrame.setStyleSheet("border: 0px")
        self.info = QVBoxLayout(self.infoFrame)
        self.infoFrame.setMinimumWidth(250)
        self.infoFrame.setLayout(self.info)

        # HERE IS THIS ONE GUY I REALLY REALLY NEED
        self.layout.addWidget(self.infoFrame, 1, 2, 1, 1)

        self.title = QFrame(self)
        self.title.setFixedHeight(40)
        self.title.setStyleSheet("border: 0px")
        self.titleLayout = QHBoxLayout(self.title)
        self.title.setLayout(self.titleLayout)
        self.titleText = QLabel()
        self.titleText.setWordWrap(True)
        self.titleText.setText("Title: ")
        self.titleText.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 14pt \"Tahoma\"; border: 0px}")
        self.info.addWidget(self.titleText)
        self.titleLine = QLabel()
        self.titleLine.setWordWrap(True)
        self.titleLine.setMinimumSize(140,30)
        self.titleLine.setText("Something")
        self.titleLine.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.titleLine.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 13pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80); border-radius: 5px")
        self.titleLayout.addWidget(self.titleLine)

        self.info.addWidget(self.title)

        self.descText = QLabel()
        self.descText.setText("Description: ")
        self.descText.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 14pt \"Tahoma\"; border: 0px}")
        self.info.addWidget(self.descText)
        self.descLine = QLabel()
        self.descLine.setWordWrap(True)
        self.descLine.setMinimumSize(140,30)
        self.descLine.setText("Something")
        self.descLine.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.descLine.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 12pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80); text-align: top")
        self.info.addWidget(self.descLine)

        self.tagsText = QLabel()
        self.tagsText.setText("Tags: ")
        self.tagsText.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 12pt \"Tahoma\"; border: 0px}")
        self.info.addWidget(self.tagsText)
        self.tagsFrame = QFrame()
        self.tagsScroll = QScrollArea()
        self.tagsScroll.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Expanding)
        self.tagsScroll.setWidgetResizable(True)
        self.tagsScroll.setWidget(self.tagsFrame)
        self.tagsLayout = QVBoxLayout(self.tagsFrame)
        self.tagsLayout.setAlignment(Qt.AlignTop)
        self.tagsFrame.setLayout(self.tagsLayout)
        self.info.addWidget(self.tagsScroll)

        self.bottom = QFrame()
        self.botLayout = QHBoxLayout(self.bottom)
        self.bottom.setLayout(self.botLayout)
        
        self.delButton = QPushButton()
        self.delButton.setFixedHeight(30)
        self.delButton.setText('Delete')
        self.delButton.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                            "font: 25 14pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                            "border: 2px solid rgb(80, 80, 80); text-align: bottom;}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180);}")
        self.botLayout.addWidget(self.delButton)

        self.editButton = QPushButton()
        self.editButton.setFixedHeight(30)
        self.editButton.setText('Edit')
        self.editButton.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                            "font: 25 14pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                            "border: 2px solid rgb(80, 80, 80); text-align: bottom;}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180);}")

        self.botLayout.addWidget(self.editButton)

        self.openButton = QPushButton()
        self.openButton.setFixedHeight(30)
        self.openButton.setText('Open')
        self.openButton.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                            "font: 25 14pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                            "border: 2px solid rgb(80, 80, 80); text-align: bottom;}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180);}")

        self.botLayout.addWidget(self.openButton)

        self.clipButton = QPushButton()
        self.clipButton.setFixedHeight(30)
        self.clipButton.setText('Copy')
        self.clipButton.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                            "font: 25 14pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                            "border: 2px solid rgb(80, 80, 80); text-align: bottom;}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180);}")

        self.botLayout.addWidget(self.clipButton)



        self.delButton.clicked.connect(self.delQueue)
        self.openButton.clicked.connect(self.openInExplorer)
        self.clipButton.clicked.connect(self.clipboard)

        # item = QSpacerItem(40, 2, QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)
        # self.info.addItem(item)
        self.info.addWidget(self.bottom)
        self.frameW = self.width()-270
        self.frameH = self.height()-25

        # Edition
        self.editFrame = QFrame()
        self.editFrame.setStyleSheet("border: 0px")
        self.editFrame.setVisible(False)
        self.edit = QVBoxLayout(self.editFrame)
        self.editDescText = QLabel()
        self.editDescText.setText("Description: ")
        self.editDescText.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 14pt \"Tahoma\"; border: 0px}")
        self.edit.addWidget(self.editDescText)
        self.descEdit = QTextEdit()
        self.descEdit.setMinimumSize(140,30)
        self.descEdit.setText("")
        self.descEdit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.MinimumExpanding)
        self.descEdit.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 12pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80); text-align: top")
        self.edit.addWidget(self.descEdit)

        self.editTagsText = QLabel()
        self.editTagsText.setText("Tags: ")
        self.editTagsText.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 14pt \"Tahoma\"; border: 0px}")
        self.edit.addWidget(self.editTagsText)
        # self.tagsEdit = QTextEdit()
        # self.tagsEdit.setMinimumSize(140,30)
        # self.tagsEdit.setText("")
        # self.tagsEdit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.MinimumExpanding)
        # self.tagsEdit.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 12pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80); text-align: top")
        # self.edit.addWidget(self.tagsEdit)
        self.textTagsFrame = QFrame()
        self.tagsFrameLayout = QVBoxLayout(self.textTagsFrame)
        self.textTagsFrame.setStyleSheet("QFrame { background-color: rgb(250, 250, 250); color: rgb(80, 80, 80); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);}")
        self.displaytagsframe = QFrame()
        self.displaytagsscroll = QScrollArea()
        self.displaytagsscroll.setStyleSheet("border: none")
        self.displaytagsscroll.setWidget(self.displaytagsframe)
        self.displaytagsscroll.setWidgetResizable(True)
        # self.displaytagsframe.setMaximumHeight(1)
        self.displaytagsscroll.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.displaytagsframe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.displaytagsLayout = QVBoxLayout(self.displaytagsframe)
        self.displaytagsLayout.setAlignment(Qt.AlignTop)
        self.textTagsField = QLineEdit(self)
        self.textTagsField.returnPressed.connect(self.proceedTag)
        self.textTagsField.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.textTagsField.setAlignment(Qt.AlignTop)
        self.textTagsField.setStyleSheet("QLineEdit {background-color: rgb(250, 250, 250);color: rgb(80, 80, 80); font: 75 10pt \"Tahoma\"; border: none; text-align: top}")
        self.tagsFrameLayout.addWidget(self.textTagsField)
        self.tagsLayout.addWidget(self.textTagsFrame)
        self.tagsFrameLayout.addWidget(self.displaytagsscroll)

        self.edit.addWidget(self.textTagsFrame)
        
        self.buttonsFrame = QFrame()
        self.buttonsFrame.setStyleSheet("border: 0px")
        self.buttonsLay = QHBoxLayout(self.buttonsFrame)
        self.confirmButton = QPushButton()
        self.confirmButton.setFixedHeight(30)
        self.confirmButton.setText('Confirm')
        self.confirmButton.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                            "font: 25 14pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                            "border: 2px solid rgb(80, 80, 80); text-align: bottom;}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180);}")
        self.buttonsLay.addWidget(self.confirmButton)

        self.cancelButton = QPushButton()
        self.cancelButton.setFixedHeight(30)
        self.cancelButton.setText('Cancel')
        self.cancelButton.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                            "font: 25 14pt \"Tahoma\"; border-radius: 5px; color: rgb(0, 0, 0);"
                            "border: 2px solid rgb(80, 80, 80); text-align: bottom;}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180);}")
        self.buttonsLay.addWidget(self.cancelButton)
        self.edit.addWidget(self.buttonsFrame)

        self.editButton.clicked.connect(self.startEdit)
        self.cancelButton.clicked.connect(self.cancelEdit)
        self.confirmButton.clicked.connect(self.confirmEdit)
        self.layout.addWidget(self.editFrame, 1, 2, 1, 1)
        self.tempTags = []

    def clipboard(self):
        print(self.picture.pic.path) 
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        cb.setPixmap(QPixmap(self.picture.pic.path), mode = cb.Clipboard)

    def proceedTag(self, tag=''):
        """
            Добавляет и отображает временный тэг в специальном окне.

            tag = str, тэг, который нужно добавить
        """
        if not tag:
            tag = self.textTagsField.text()
        check = True
        for i in range(1):
            for c in SPECIAL_SYMBOLS:
                if c in tag:
                    check = False
                    break
            if not check:
                break
            # tag = tag.replace(' ', '_')
            self.tempTags.append(TagLabel(self.displaytagsframe, tag, '0x77745'))
            self.tempTags[-1].bye = False
            self.tempTags[-1].mousePressEvent = None
            self.tempTags[-1].label.setWordWrap(False)
            self.tempTags[-1].deleteit = self.removeTempTag
            self.tempTags[-1].setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
            self.tempTags[-1].label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
            self.displaytagsLayout.addWidget(self.tempTags[-1])
            # self.spacechecker()
            self.textTagsField.clear()

    # def spacechecker(self):
    #     if len(self.tempTags) > 0:
    #         self.displaytagsframe.setMaximumHeight(200)


    def removeTempTag(self, all=False):
        """
            Удаляет временные тэги отмеченные или все сразу

            all = bool, удалить всё или нет
        """
        if not all:
            for tag in range(len(self.tempTags)):
                if self.tempTags[tag].bye:
                    self.tempTags[tag].deleteLater()
                    self.tempTags.pop(tag)
                    break
        elif all:
            for tag in self.tempTags:
                tag.deleteLater()
            self.tempTags = []

    def startEdit(self):
        """
            Функция, начинающая редактирование картинки
        """
        self.infoFrame.setVisible(False)
        self.editFrame.setVisible(True)
        self.descEdit.setText(self.picture.pic.description)
        # tags = ''
        for tag in self.picture.texttags:
            self.proceedTag(tag)
        # self.textTagsField.setText(tags)

    def cancelEdit(self):
        """
            функция, которая отменяет редактирование, и возвращает к картинке
        """
        self.editFrame.setVisible(False)
        self.removeTempTag(all=True)
        self.infoFrame.setVisible(True)

    def confirmEdit(self):
        self.setVisible(False)
        # path = self.picture.pic.path
        desc = self.descEdit.toPlainText()
        tags = ''
        for tag in self.tempTags:
            tags += '#' + tag.text
        tags = tags.replace(' ', '_')
        tags = [tags.split('#')]
        self.reader.setDescription(self.picture.pic, desc)
        self.picture.pic.description = desc
        self.reader.setTags(self.picture.pic, tags[0][1:])
        self.picture.pic.tagsID = tags[0][1:]
        self.picture.wipeTheTags()
        self.picture.displayTags()
        self.closeSequence()
      
    def show(self, pic):
        """
            Функция, которая отображает картинку на окне

            pic = Picture, объект картинки
        """
        self.picture = pic
        self.titleLine.setText(self.picture.pic.path[self.picture.pic.path.rfind("/")+1:])
        self.descLine.setText(self.picture.pic.description)

        self.displayPic()

    def hide(self):
        """
            Скрывает большое окно, очищает поля и картинку.
        """
        self.cancelEdit()
        self.setVisible(False)
        if self.picture:
            self.picture.redisplaytags()
            self.picture = None
        # print(">>> Cleared Picture")
        self.titleLine.setText("")
        self.descLine.setText("")
        self.removeTempTag(all=True)
        self.image.clear()

    def delQueue(self):
        """
            Функция удаление картинки и после закрытия большого окна
        """
        self.picture.toDelete()
        self.closeSequence()
    
    def openFile(self):
        """
            А где блин функция, которая здесь была???!
            А что здесь было..?
        """
        pass

    def displayPic(self):
        """
            Отображает картинку и тэги картинки.
        """
        for tag in self.picture.tags:
            self.tagsLayout.addWidget(tag)
        self.im = QPixmap(self.picture.pic.path)
        self.image.setPixmap(self.im.scaled(self.frameW, self.frameH, Qt.KeepAspectRatio, Qt.SmoothTransformation))
    
    def getSize(self): 
        """
            Функция изменяет разрешение картинки, чтобы оно подходило под окно
        """
        self.frameW = self.width()-320
        self.frameH = self.height()-25
        if self.picture:
            self.image.setPixmap(self.im.scaled(self.frameW, self.frameH, Qt.KeepAspectRatio, Qt.SmoothTransformation))

    def newSize(self, globalx, globaly):
        """
            Обновляет размер таба при изменении
            размера основного окна.

            globalx = int, текущая необработанная ширина окна.
            globaly = int, текущая необработанная высота окна.
        """
        self.resize(globalx - 200, globaly - 200)
        self.getSize()

    def openInExplorer(self):
        """
            Функция открывает выбранную картинку в проводнике.
        """
        path = self.picture.pic.path.replace('/', '\\')
        subprocess.Popen(f'explorer /select,{path}')