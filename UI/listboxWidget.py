from PyQt5.QtCore import pyqtSignal, Qt, QSize
from PyQt5.QtWidgets import QListWidget, QGridLayout, QHBoxLayout, QLabel, QLineEdit, \
    QVBoxLayout, QSizePolicy, QPushButton, QFrame, QWidget, QTextEdit, QFileDialog, QSpacerItem, QComboBox, QScrollArea
from PyQt5.QtGui import QPixmap, QKeyEvent
from UI.imageLabel import TagLabel as TagL
import UI.mainWindow as mWin
import os, sys

from UI.help import texts, links

HOR_SIZE = 400
SUPPORTED = ('.jpeg', '.jpg', '.png')
SPECIAL_SYMBOLS = ('&', '#', '-')



class addTab(QFrame):
    """
        Класс отвечает за отбражение выдвигающегося окна с добавлением мемов.
    """
    def __init__(self, parent, verticalSize, coordinates):
        """
            parent = Window, объект класса, которому принадлежит таб 
            verticalSize = int, текущая высота основного окна
            coordinates = tuple of ints, координаты, на которые будет приходиться верхний левый угол таба.
        """
        super(addTab, self).__init__(parent)

        # Creating variables
        self.textTags = list()

        # Initialization of the Frame
        self.resize(HOR_SIZE, verticalSize) 
        self.move(*coordinates)
        self.setVisible(False)
        self.shown = False
        self.setStyleSheet("QFrame { background-color: rgb(234,234,234); border: 2px solid rgb(80, 80, 80); }")
        self.layout = QGridLayout(self)
        # self.layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.layout)

        # Adding drop Field
        self.drops = PictureDropField()
        self.drops.setAcceptDrops(True)
        self.drops.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.layout.addWidget(self.drops, 0, 0, 2, 2)

        # Creating a line for the title and Accept button
        self.title = QFrame(self)
        self.title.setFixedHeight(40)
        self.title.setStyleSheet("border: 0px")
        self.titleLayout = QHBoxLayout(self.title)
        self.title.setLayout(self.titleLayout)
        self.titleText = QLabel()
        self.titleText.setText("Title: ")
        self.titleText.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 13pt \"Tahoma\"; border: 0px}")
        self.titleLayout.addWidget(self.titleText)
        self.titleLine = QLineEdit()
        # self.titleLine.setText("This Field has been suspended")
        # self.titleLine.setReadOnly(True)
        self.titleLine.setMinimumSize(150,30)
        self.titleLine.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.titleLine.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);")
        self.titleLayout.addWidget(self.titleLine)
        self.acceptBtn = QPushButton("Accept")
        self.acceptBtn.setFixedSize(100,30)
        self.acceptBtn.setStyleSheet("QPushButton { background-color: rgb(220, 220, 220); "
                            "font: 25 10pt \"Tahoma\"; border-radius: 10px; color: rgb(0, 0, 0); "
                            "border: 2px solid rgb(80, 80, 80)}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180); }")
        self.titleLayout.addWidget(self.acceptBtn)

        self.layout.addWidget(self.title, 2, 0, 1, 2)
        
        # Creating the description Field
        self.description = QFrame()
        self.description.setMaximumHeight(400)
        self.description.setStyleSheet("border: 0px")
        self.desLayout = QVBoxLayout(self.description)
        self.description.setLayout(self.desLayout)

        self.desLabel = QLabel()
        self.desLabel.setText("Description:")
        self.desLabel.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 15pt \"Tahoma\"; border: 0px}")
        self.desLayout.addWidget(self.desLabel)

        self.desText = QTextEdit()
        self.desText.setStyleSheet("QTextEdit { background-color: rgb(250, 250, 250); color: rgb(80, 80, 80); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80)}")
        self.desLayout.addWidget(self.desText)

        self.layout.addWidget(self.description, 3, 0, 1, 1)


        # Creating the Tags field
        self.tagsField = QFrame()
        self.tagsField.setMaximumHeight(400)
        self.tagsField.setMinimumWidth(200)
        self.tagsField.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.tagsField.setStyleSheet("border: 0px")
        self.tagsLayout = QVBoxLayout()
        self.tagsField.setLayout(self.tagsLayout)

        self.tagsLabel = QLabel()
        self.tagsLabel.setText("Tags:")
        self.tagsLabel.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 15pt \"Tahoma\"; border: 0px}")
        self.tagsLayout.addWidget(self.tagsLabel)

        self.textTagsFrame = QFrame()
        self.tagsFrameLayout = QVBoxLayout(self.textTagsFrame)
        self.textTagsFrame.setStyleSheet("QFrame { background-color: rgb(250, 250, 250); color: rgb(80, 80, 80); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);}")
        self.displaytagsframe = QFrame()
        self.displaytagsscroll = QScrollArea()
        self.displaytagsscroll.setStyleSheet("border: none")
        self.displaytagsscroll.setWidget(self.displaytagsframe)
        self.displaytagsscroll.setWidgetResizable(True)
        # self.displaytagsframe.setMaximumHeight(1)
        self.displaytagsscroll.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.displaytagsframe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.displaytagsLayout = QVBoxLayout(self.displaytagsframe)
        self.displaytagsLayout.setAlignment(Qt.AlignTop)
        self.textTagsField = QLineEdit(self)
        self.textTagsField.returnPressed.connect(self.proceedTag)
        self.textTagsField.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.textTagsField.setAlignment(Qt.AlignTop)
        self.textTagsField.setStyleSheet("QLineEdit {background-color: rgb(250, 250, 250);color: rgb(80, 80, 80); font: 75 10pt \"Tahoma\"; border: none; text-align: top}")
        self.tagsFrameLayout.addWidget(self.textTagsField)
        self.tagsLayout.addWidget(self.textTagsFrame)
        self.tagsFrameLayout.addWidget(self.displaytagsscroll)

        self.tempTags = []
        self.layout.addWidget(self.tagsField, 3, 1, 1, 1)

    def proceedTag(self):
        """
            Добавляет и отображает временный тэг в специальном окне.
        """
        tag = self.textTagsField.text()
        check = True
        for i in range(1):
            for c in SPECIAL_SYMBOLS:
                if c in tag:
                    check = False
                    break
            if not check:
                break
            # tag = tag.replace(' ', '_')
            self.tempTags.append(TagL(self.displaytagsframe, tag, '0x77745'))
            self.tempTags[-1].bye = False
            self.tempTags[-1].mousePressEvent = None
            self.tempTags[-1].label.setWordWrap(False)
            self.tempTags[-1].deleteit = self.removeTempTag
            self.tempTags[-1].setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
            self.tempTags[-1].label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
            self.displaytagsLayout.addWidget(self.tempTags[-1])
            # self.spacechecker()
            self.textTagsField.clear()

    # def spacechecker(self):
    #     if len(self.tempTags) > 0:
    #         self.displaytagsframe.setMaximumHeight(200)


    def removeTempTag(self, all=False):
        """
            Удаляет временные тэги отмеченные или все сразу

            all = bool, удалить всё или нет
        """
        if not all:
            for tag in range(len(self.tempTags)):
                if self.tempTags[tag].bye:
                    self.tempTags[tag].deleteLater()
                    self.tempTags.pop(tag)
                    break
        elif all:
            for tag in self.tempTags:
                tag.deleteLater()
            self.tempTags = []


    def newSize(self, height):
        """
            Обновляет размер таба при изменении
            размера основного окна.

            height = int, текущий размер основного окна.
        """
        self.resize(HOR_SIZE, height-20)

    def addTags(self):
        """
            Берет тэги из соответствующего поля и отправляет их
            в соответствующую переменную.
        """
        tags = ''
        for tag in self.tempTags:
            tags += '#' + tag.text
        # tags = self.textTagsField.toPlainText()
        tags = tags.replace(' ', '_')
        self.textTags.append(tags.split("#"))
        self.textTags = self.textTags[0][1:]
        self.removeTempTag(all=True)

    def fieldsClear(self):
        """
            Очищает все поля конструктора мемов.
        """
        # self.titleLine.clear()
        self.desText.clear()
        self.textTagsField.clear()


class PictureDropField(QLabel):
    """
        Класс, который отвечает за окно добавление картинок,
        позволяя их выбрать или перетащить в соответствующее окно.
    """
    def __init__(self):
        super(PictureDropField, self).__init__()
        self.links = list()

        self.setAcceptDrops(True)
        self.setStyleSheet("QLabel {border: 2px dashed rgb(80, 80, 80);}")
        self.setMinimumHeight(200)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.MinimumExpanding)

        self.layout = QVBoxLayout(self)
        self.setLayout(self.layout)

        self.textDrag = QLabel()
        self.textDrag.setAlignment(Qt.AlignCenter)
        self.textDrag.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 15pt \"Tahoma\"; border: 0px}")
        self.textDrag.setText("Drag and drop\nyour meme\nor")
        self.layout.addWidget(self.textDrag)

        self.addBtn = mWin.Button("ADD IMAGE")
        self.addBtn.setFixedSize(150, 50)
        self.layout.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.addBtn.setStyleSheet("QPushButton { background-color: rgb(250, 250, 250);"
                            "font: 25 10pt \"Tahoma\"; border-radius: 10px; color: rgb(0, 0, 0);"
                            "border: 2px solid rgb(80, 80, 80)}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180); }")
        self.layout.addWidget(self.addBtn)
        self.addBtn.clicked.connect(self.chooseImage)


    def dragEnterEvent(self, event):
        """
            Отслеживает, когда курсор содержит файлы.

            event = ?, qt сам передает переменную
        """
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        """
            Отслеживает, если курсор с файлами находится в поле.

            event = ?, qt сам передает переменную
        """
        if event.mimeData().hasUrls:
            event.setDropAction(Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """
            Если курсор сбросил файлы - функция их обрабатывает и добавляет
            в соответствующий список.

            event = ?, qt сам передает переменную
        """
        if event.mimeData().hasUrls:
            event.setDropAction(Qt.CopyAction)
            event.accept()
            for url in event.mimeData().urls():
                self.links.append(str(url.toLocalFile()))
            print('\n <<<<<<<<<<<<<<<<<<<<<<<', self.links)
            self.refreshPicture()
        else:
            event.ignore()

    def chooseImage(self):
        """
            Открывает окно выбора картинки из проводника и, если
            что-то было выбрано, добавляет в очередь.
        """
        file = QFileDialog.getOpenFileName(parent=self, caption="choose img")
        if file[0]:
            index = file[0].rfind('.')
            if (file[0][index:].lower() in SUPPORTED) and (index != -1):
                self.links.append(file[0])
                print(file[0])
                self.refreshPicture()

    def refreshPicture(self):
        """
            Отображает название следующего в очереди файла.
        """
        if self.links:
            pic = self.links[0][self.links[0].rfind('/')+1:]
            self.addBtn.setText(pic)
            
        elif not self.links:
            self.addBtn.setText("ADD IMAGE")


class slidingTagsTab(QFrame):
    """
        Класс отвечает за отбражение выдвигающегося окна с существующими тэгами.
    """
    def __init__(self, parent, verticalSize, coordinates):
        """
            parent = Window, объект класса, которому принадлежит таб 
            verticalSize = int, текущая высота основного окна
            coordinates = tuple of ints, координаты, на которые будет приходиться верхний левый угол таба.
        """
        super(slidingTagsTab, self).__init__(parent)
        self.resize(HOR_SIZE, verticalSize) 
        self.move(*coordinates)
        self.setVisible(False)
        self.shown = False
        self.setStyleSheet("QFrame { background-color: rgb(234,234,234); border: 2px solid rgb(80, 80, 80); }")
        self.layout = QGridLayout(self)
        # self.layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.layout)

        self.tagsField = QFrame()
        self.tagsField.setMaximumHeight(verticalSize)
        self.tagsField.setMinimumWidth(350)
        self.tagsField.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.tagsField.setStyleSheet("border: 0px")
        self.tagsLayout = QVBoxLayout()
        self.tagsField.setLayout(self.tagsLayout)


        self.tagsLabel = QLabel()
        self.tagsLabel.setText("Tags:")
        self.tagsLabel.setStyleSheet("QLabel { color: rgb(80, 80, 80); font: 75 15pt \"Tahoma\"; border: 0px}")
        self.tagsLayout.addWidget(self.tagsLabel)

        self.textTagsFrame = QFrame()
        self.tagsFrameLayout = QVBoxLayout(self.textTagsFrame)
        self.textTagsFrame.setStyleSheet("QFrame { background-color: rgb(250, 250, 250); color: rgb(80, 80, 80); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);}")
        self.tagList = QFrame()
        self.displaytagsscroll = QScrollArea()
        self.displaytagsscroll.setStyleSheet("border: none")
        self.displaytagsscroll.setWidget(self.tagList)
        self.displaytagsscroll.setWidgetResizable(True)
        # self.displaytagsframe.setMaximumHeight(1)
        self.displaytagsscroll.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.tagList.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.listLayout = QVBoxLayout(self.tagList)
        self.listLayout.setAlignment(Qt.AlignTop)
        self.tagsLayout.addWidget(self.textTagsFrame)
        self.tagsFrameLayout.addWidget(self.displaytagsscroll)

        self.layout.addWidget(self.tagsField, 0, 0, 0, 0)



    def newSize(self, height):
        """
            Обновляет размер таба при изменении
            размера основного окна.

            height = int, текущий размер основного окна.
        """
        self.resize(HOR_SIZE, height-20)

class closingShiet(QFrame):
    """
        Класс, который создает затемненную область за остальными табами,
        при нажатии на который все табы закрываются.
    """
    def __init__(self, parent, sizes, coordinates, closeEverything):
        """
            parent = Window, объект основного окна, которому принадлежит таб
            sizes = tuple of int, текущие, уже обработанные размеры окна в кортеже
            coordinates = tuple of int, координаты верхнего левого угла, куда ставится этот таб
            closeEverything = func, функция извне, которая закрывает все табы
        """
        super(closingShiet, self).__init__(parent)

        self.mousePressEvent = self.superClose
        self.closeEverything = closeEverything
        self.resize(*sizes)
        self.move(*coordinates)
        self.setVisible(False)
        self.shown = False
        self.setStyleSheet("background-color: rgba(0, 0, 0, 60)")

    def newSize(self, coords):
        """
            Обновляет размер таба при изменении
            размера основного окна.

            height = int, текущий размер основного окна.
        """
        self.resize(coords[0]-91, coords[1]-24)

    def close(self):
        """
            Функция, которая скрывает данный таб
        """
        self.setVisible(False)
        self.shown = False

    def open(self):
        """
            Функция, которая показывает данный таб
        """
        self.setVisible(True)
        self.shown = True
    
    def superClose(self, event):
        """
            Функция, которая закрывает все табы

            event = ?, qt передает это сам.
        """
        self.closeEverything()


class SettingsTab(QFrame):
    """
        Класс, который открывает окно настроек.
    """
    def __init__(self, parent, sizes, coordinates, reader):
        """
            parent = Window, объект основного окна, которому принадлежит таб
            sizes = tuple of int, текущие, уже обработанные размеры окна в кортеже
            coordinates = tuple of int, координаты верхнего левого угла, куда ставится этот таб
            reader = FileReader, доступ к json файлам, чтобы сохранить настройки
        """
        super(SettingsTab, self).__init__(parent)

        # Initialization of the Frame
        self.reader = reader
        self.resize(*sizes)
        self.move(*coordinates)
        self.setVisible(False)
        self.shown = False
        self.setStyleSheet("QFrame { background-color: rgb(234,234,234); border: 2px solid rgb(80, 80, 80); }")
        self.layout = QVBoxLayout(self)
        # self.layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.layout)

        self.uNameFrame = QFrame()
        self.uNameLayout = QHBoxLayout(self.uNameFrame)
        self.uNameFrame.setStyleSheet("border: 0px")
        self.uText = QLabel()
        self.uText.setStyleSheet("QLabel { color: rgb(0, 0, 0); font: 75 13pt \"Tahoma\"; border: 0px}")
        self.uText.setText("Username: ")
        self.username = QLineEdit()
        self.username.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);")
        self.username.setReadOnly(True)
        self.uname = True
        self.username.setText("TestSubject")
        self.uNameLayout.addWidget(self.uText)
        self.uNameLayout.addWidget(self.username)
        self.notMe = QLabel()
        self.notMe.setStyleSheet("QLabel { color: rgb(0, 0, 0); font: 75 8pt \"Tahoma\"; border: 0px; text-decoration: underline}")
        self.notMe.setText("not me")
        self.notMe.mousePressEvent = self.unlockUsername
        self.uNameLayout.addWidget(self.notMe)
        spacer = QSpacerItem(20, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.uNameLayout.addItem(spacer)

        self.ignoreFrame = QFrame()
        self.ignoreLayout = QVBoxLayout(self.ignoreFrame)
        self.ignoreFrame.setStyleSheet("border: 0px")
        self.ignoreLabel = QLabel()
        self.ignoreLabel.setStyleSheet("QLabel { color: rgb(0, 0, 0); font: 75 13pt \"Tahoma\"; border: 0px}")
        self.ignoreLabel.setWordWrap(True)
        self.ignoreLabel.setText("We need usernames to work with memes from other person. You can exclude memes from a specific person by typing his username in the form below:")
        self.ignoreLayout.addWidget(self.ignoreLabel)
        self.ignoreForm = QTextEdit()
        self.ignoreForm.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);")
        self.ignoreForm.setText("Something")
        self.ignoreLayout.addWidget(self.ignoreForm)

        self.languageFrame = QFrame()
        self.languageFrame.setStyleSheet("border: 0px")
        self.languageLayout = QHBoxLayout(self.languageFrame)
        self.languageLabel = QLabel()
        self.languageLabel.setStyleSheet("QLabel { color: rgb(0, 0, 0); font: 75 13pt \"Tahoma\"; border: 0px}")
        self.languageLabel.setText("Language: ")
        self.languageLayout.addWidget(self.languageLabel)
        self.languageChoose = QComboBox()
        self.languageChoose.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.languageChoose.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);")
        self.languageLayout.addWidget(self.languageChoose)
        self.languageChoose.setMinimumSize(100, 25)
        self.languageChoose.addItem("English")
        spacer = QSpacerItem(20, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.languageLayout.addItem(spacer)

        self.buttonFrame = QFrame()
        self.buttonFrame.setStyleSheet("border: 0px")
        self.buttonLayout = QHBoxLayout(self.buttonFrame)
        self.saveButton = QPushButton()
        self.saveButton.setFixedSize(200, 50)
        self.saveButton.setText('Save Changes')
        self.saveButton.setStyleSheet("QPushButton { background-color: rgb(200, 200, 200);"
                            "font: 25 14pt \"Tahoma\"; border-radius: 2px; color: rgb(0, 0, 0);"
                            "border: 1px solid rgb(80, 80, 80); text-align: center;}\n"
                            "QPushButton:pressed { background-color: rgb(180,180,180);}")
        spacer = QSpacerItem(20, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.buttonLayout.addItem(spacer)
        self.saveButton.clicked.connect(self.saveValues)
        self.buttonLayout.addWidget(self.saveButton)

        self.layout.addWidget(self.uNameFrame)
        self.layout.addWidget(QHLine())
        self.layout.addWidget(self.ignoreFrame)
        self.layout.addWidget(QHLine())
        self.layout.addWidget(self.languageFrame)
        # self.layout.addWidget(QHLine())
        self.layout.addWidget(self.buttonFrame)


        spacer = QSpacerItem(20, 10, QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.layout.addItem(spacer)


    def newSize(self, coords):
        """
            Обновляет размер таба при изменении
            размера основного окна.

            height = int, текущий размер основного окна.
        """
        self.resize(coords[0]-91, coords[1]-24)

    def unlockUsername(self, dump):
        """
            Функция, которая позволяет редактировать имя пользователя.
            На данный момент не имеет особого применения.

            dump = ?, event, который не нужен, но он существует.
        """
        self.username.setReadOnly(not self.uname)
        self.uname = not self.uname

    def setValues(self):
        """
            Выставляет текущие значения в полях
        """
        info = self.reader.getSettings()

        self.username.setText(info['username'])
        ignorelist = ''
        if len(info['ignored']):
            for i in info['ignored']:
                ignorelist += i + ';'
            ignorelist = ignorelist[:-1]
        self.ignoreForm.setText(ignorelist)

        index = self.languageChoose.findText(info['language'], Qt.MatchFixedString)
        if index >= 0:
            self.languageChoose.setCurrentIndex(index)
            

    def saveValues(self):
        """
            Сохраняет новые значения в json файл.
        """
        username = self.username.text()

        language = str(self.languageChoose.currentText())
        self.reader.changeLanguage(language)

        ignored = self.ignoreForm.toPlainText()
        ignored = ignored.split(";")
        self.reader.clearIgnore()
        
        for user in ignored:
            if user:
                self.reader.addIgnore(user)



class HelpTab(QFrame):
    """
        Класс, который создает окно для краткой справки для пользователя
    """
    def __init__(self, parent, verticalSize, coordinates):
        """
            parent = Window, объект основного окна, которому принадлежит таб
            sizes = tuple of int, текущие, уже обработанные размеры окна в кортеже
            coordinates = tuple of int, координаты верхнего левого угла, куда ставится этот таб
        """
        super(HelpTab, self).__init__(parent)

        # Initialization of the Frame
        self.resize(HOR_SIZE+100, verticalSize)
        self.move(*coordinates)
        self.setVisible(False)
        self.shown = False
        self.setStyleSheet("QFrame { background-color: rgb(234,234,234); border: 2px solid rgb(80, 80, 80); }")
        self.layout = QVBoxLayout(self)
        # self.layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.layout)

        self.mainScroll = QScrollArea()
        self.mainScroll.setWidgetResizable(True)
        self.mainFrame = QFrame()
        self.mainFrame.setStyleSheet("QFrame { background-color: rgb(250, 250, 250); color: rgb(80, 80, 80); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);}")
        self.mainScroll.setWidget(self.mainFrame)
        self.mainLayout = QVBoxLayout(self.mainFrame)

        self.texty = []
        self.picty = []
        
        self.newLabel(texts['text1'])
        self.newPic(links['link1'])
        self.newLabel(texts['text2'])
        self.newPic(links['link2'])
        self.newLabel(texts['text3'])
        self.newPic(links['link3'])
        self.newLabel(texts['text4'])
        self.newPic(links['link4'])
        self.newPic(links['link5'])
        self.newPic(links['link6'])
        self.newLabel(texts['text5'])
        self.newPic(links['link7'])
        self.newPic(links['link8']) 
        self.newLabel(texts['text6'])
        self.newPic(links['link9'])
        self.newLabel(texts['text7'])
        self.newLabel(texts['text8'])
        self.newPic(links['link10'])
        self.newLabel(texts['text9'])
        self.newPic(links['link11'])
        self.newLabel(texts['text10'])
        self.newPic(links['link12'])
        self.newLabel(texts['text11'])
        self.newLabel(texts['text12'])
        self.newPic(links['link13'])
        self.newLabel(texts['text13'])



        self.layout.addWidget(self.mainScroll)

    def newLabel(self, txt):
        """
            Клипает текст

            txt = str, текст, который надо добавить.
        """
        lb = QLabel()
        lb.setStyleSheet("background-color: rgb(250, 250, 250); font: 75 10pt \"Tahoma\"; border: 1px solid rgb(80, 80, 80);")
        lb.setText(txt)
        lb.setWordWrap(True)
        self.texty.append(lb)
        self.mainLayout.addWidget(self.texty[-1])
    
    def newPic(self, path):
        """
            Клипает картинки

            path = str, путь к картинке
        """
        pc = QLabel()
        pc.setPixmap(QPixmap(resource_path(path)))
        pc.setFixedWidth(435)
        pc.setFixedHeight(357)
        pc.setScaledContents(True)
        self.picty.append(pc)
        self.mainLayout.addWidget(self.picty[-1])

    def newSize(self, height):
        """
            Обновляет размер таба при изменении
            размера основного окна.

            height = int, текущий размер основного окна.
        """
        self.resize(HOR_SIZE+100, height-20)

class QHLine(QFrame):
    """
        Класс, который рисует крутую горизонтальную линию.
    """
    def __init__(self):
        super(QHLine, self).__init__()
        # self.setStyleSheet("border: 1px")
        self.setFrameShape(QFrame.HLine)
        self.setFrameShadow(QFrame.Sunken)

def resource_path(relative_path):
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)