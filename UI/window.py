import time
from functools import wraps
from sys import exit as sysexit, exc_info
from threading import Thread

from PyQt5.QtCore import Qt, QMutex, QObject, QThread
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QFileDialog, QMainWindow, QSpacerItem, QSizePolicy, QSizeGrip, QApplication
import UI.mainWindow as mWin

from UI.imageLabel import ImageLabel, TagLabel
from UI.listboxWidget import addTab, slidingTagsTab, closingShiet, SettingsTab, HelpTab
from UI.bigPicture import mFrame
from jsonLib import FileReader, Picture

import zipfile
import os

# Constants
SUPPORTED = ('.jpeg', '.jpg', '.png')  # Currently supported extensions
PIXEL_LIST = (535, 798, 1061, 1324, 1587, 1850, 2113, 2376, 2639, 2902, 3165)


class Grip(QSizeGrip):
    """
        Класс, который создает специальную позицию для изменения размера окна.
    """
    s1 = pyqtSignal()

    def __init__(self, parent):
        """
            parent = Window, объект основного окна
        """
        super(Grip, self).__init__(parent)
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.setFixedHeight(6)

    def mousePressEvent(self, event):
        """
            Фукнция отслеживает, если на иконку нажали и передает
            первоначальные координаты окна.

            event = ?, qt сам там всё передает, не парьтесь
        """
        self.offset = event.pos()

    def mouseMoveEvent(self, event):
        """
            Функция отслеживает движения нажатого курсора и передает новые координаты.

            event = ?, qt сам там всё передает, не парьтесь
        """
        x = event.globalX()
        y = event.globalY()
        self.delta = (x, y)
        self.s1.emit()


class Window(QMainWindow):
    """
        Сердце программы. У меня всё.
    """
    s1 = pyqtSignal()

    def __init__(self, mode):
        """
            mode = ?, первый раз вижу эту переменную.
        """
        super(Window, self).__init__()
        self.ui = mWin.UiMainWindow()
        self.ui.setupUi(self)
        flags = Qt.WindowFlags(Qt.FramelessWindowHint)
        self.setWindowFlags(flags)
        self.currentTag = None
        self.tagsCount = 0

        self.labels = {'all': []}
        self.currReq = 'all'
        self.count = {'x': 0, 'y': 0}
        self.memesInRow = 2
        self.longest = 0
        self.potentialLength = 0
        self.vspacers = []
        self.hspacers = []
        self.threads = []

        self.sizegrip = Grip(self)
        self.ui.gridLayout_2.addWidget(self.sizegrip, 3, 0, 1, 1, Qt.AlignBottom | Qt.AlignRight)

        # Loading saved images
        self.fileReader = FileReader()

        # Tabs Init
        self.sitStillImaCloseIt = closingShiet(self, (self.width() - 90, self.height() - 23),
                                               (self.pos().x() + 90, self.pos().y() + 23), self.closeAll)
        self.settings = SettingsTab(self, (self.width() - 90, self.height() - 23),
                                    (self.pos().x() + 90, self.pos().y() + 23), self.fileReader)
        self.tagsTab = slidingTagsTab(self, self.height() - 20, (self.pos().x() + 90, self.pos().y() + 20))
        self.pictureAddTab = addTab(self, self.height() - 20, (self.pos().x() + 90, self.pos().y() + 20))
        self.help = HelpTab(self, self.height() - 20, (self.pos().x() + 90, self.pos().y() + 20))
        self.nicewall = mFrame(self, self.width(), self.height(), (150, 100), self.closeAll, self.fileReader)

        # Declaring some of the properties

        # Assigning functions on triggers
        self.pictureAddTab.acceptBtn.clicked.connect(self.addAcceptImage)
        self.ui.TopFrame.frame.s1.connect(self.moveWin)
        self.ui.bAdd.clicked.connect(self.hideAddTab)
        self.ui.bHelp.clicked.connect(self.showHelpWdgt)
        self.ui.bHelp.setToolTip("Opens Help tab")
        self.ui.bAdd.setToolTip("Shows adding tab")
        self.ui.bHome.clicked.connect(self.closeAll)
        self.ui.bHome.setToolTip("Closes opened tab")
        self.ui.bTags.clicked.connect(self.showTagsWdgt)
        self.ui.bTags.setToolTip("Shows tag tab")
        self.ui.bSettings.clicked.connect(self.openSettings)
        self.ui.bSettings.setToolTip("Settings")
        self.s1.connect(self.refreshTags)
        self.ui.TopFrame.Close.clicked.connect(self.exitEvent)
        self.ui.TopFrame.Minimize.clicked.connect(self.minimizeEvent)
        self.ui.TopFrame.Maximize.clicked.connect(self.maximizeEvent)
        self.sizegrip.s1.connect(self.winResize)
        self.ui.bExport.clicked.connect(self.getZip)
        self.ui.bExport.setToolTip("Export data")

        self.ui.searchButton.clicked.connect(self.search)
        self.ui.searchLine.editingFinished.connect(self.search)

        self.signal = self.ui.memesLayout.scrollContentsBy
        self.ui.memesLayout.scrollContentsBy = self.check
        self.currScrollPosition = 0
        self.currStopPosition = 240

        if mode != "TEST":
            self.showPictures()

        tmpTag = self.fileReader.loadNextTag(True)
        if tmpTag is not None:
            self.currentTag = TagLabel(self, text=tmpTag.name, showTag=self.addSearchTag)
            self.tagsTab.listLayout.addWidget(self.currentTag)

        while tmpTag is not None:
            tmpTag = self.fileReader.loadNextTag()
            if tmpTag is not None:
                self.currentTag = TagLabel(self, text=tmpTag.name, showTag=self.addSearchTag)
                self.tagsTab.listLayout.addWidget(self.currentTag)

        self.currentTag = None

        # self.longestLine()
        self.checkLines(916)

    """        self.dumb()

        def dumb(self):
            self.dumb2()
            while True:
                print(1)

        @run
        def dumb2(self):
            for i in range(1, 100000):
                print(i)
    """

    def check(self, a, b):
        self.signal(0, 0)
        self.currScrollPosition += b
        # print(-self.currScrollPosition, self.currStopPosition)
        while -self.currScrollPosition >= self.currStopPosition:
            self.showPictures(False)
            self.currStopPosition += 900

    def editImage(self, path, desc, tags):
        """
            Функция, которая сохраняет редактирование картинки

            path = str, путь картики
            desc = str, новой описание
            tags = str, новый набор тэгов
        """
        #     self.pictureAddTab.drops.links.append(path)
        #     self.pictureAddTab.desText.setText(desc)
        #     self.pictureAddTab.textTagsField.append(tags.split("#"))
        self.rearrange()

    #     self.addAcceptImage()

    def getZip(self):
        arch = QFileDialog.getSaveFileName(parent=self, caption="choose file", filter="archive (*.zip)")
        if arch[0] != "":
            self.zipFolder = zipfile.ZipFile(arch[0], "w")
            for folder, subFolders, files in os.walk(self.fileReader._currentDir + "\\data"):
                for file in files:
                    self.zipFolder.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder, file),
                                                                                     self.fileReader._currentDir + "\\data"),
                                         compress_type=zipfile.ZIP_DEFLATED)
            self.zipFolder.close()

    def hideAddTab(self):
        """
            Функция, которая скрывает и показывает таб добавления картинки.
        """
        if self.pictureAddTab.shown:
            self.closeAll()
        else:
            self.closeAll()
            self.sitStillImaCloseIt.open()
            self.pictureAddTab.setVisible(True)
            self.pictureAddTab.shown = True

    def openSettings(self):
        """
            Функция, которая скрывает и показывает таб настроек.
        """
        if self.settings.shown:
            self.closeAll()
        else:
            self.closeAll()
            self.sitStillImaCloseIt.open()
            self.settings.setValues()
            self.settings.setVisible(True)
            self.settings.shown = True

    def winResize(self):
        """
            Фукнция отслеживает изменение размеров окна и передает новые размеры.
        """
        self.resize(self.sizegrip.delta[0] - self.pos().x(), self.sizegrip.delta[1] - self.pos().y())

    def moveWin(self):
        """
            Функция передвигает окно на новые координаты на экране.
        """
        self.move(*self.ui.TopFrame.frame.to)

    def showPictures(self, fromStart=True):
        """
            Отображение необходимых, соответствующих запросу картинок.

            fromStart = bool, начать сначала
        """
        request = self.ui.searchLine.text()
        if self.currReq == "":
            self.currReq = "all"
        currPic, ass = self.fileReader.loadNextPicture(fromStart=fromStart, request=request)
        self.labels[request] = []
        QApplication.processEvents()
        for i in range(1, 15):
            if ass == 1:
                break
            if currPic is not None:
                QApplication.processEvents()
                self.addImage(currPic, request=request)
                # self.rearrange()
            currPic, ass = self.fileReader.loadNextPicture(request=request)
        self.rearrange()

    def search(self):
        """
            метод, запускающий поиск
        """
        del self.labels[self.currReq]
        self.labels["all"] = []
        self.currReq = self.ui.searchLine.text()
        self.currStopPosition = 240
        # self.currScrollPosition = 0
        self.showPictures()

    def exitEvent(self):
        """
            Закрытие программы.
        """
        sysexit()

    def minimizeEvent(self):
        """
            Сворачивание окна.
        """
        self.showNormal()
        self.showMinimized()

    def maximizeEvent(self):
        """
            Открытие окна на весь экран и обратно.
        """
        if self.isMaximized():
            self.showNormal()
        else:
            self.showMaximized()

    def addSearchTag(self, text):
        if self.ui.searchLine.text() == "":
            self.ui.searchLine.insert(text)
        else:
            self.ui.searchLine.insert(" ")
            self.ui.searchLine.insert(text)

    def resizeEvent(self, event):
        """
            Изменяет размер всего, что надо изменить.

            event = ?, qt сам там всё передает, не парьтесь
        """
        h = self.height()
        w = self.width()
        self.checkLines(self.ui.memesLayout.frameGeometry().width())
        self.pictureAddTab.newSize(h)
        self.tagsTab.newSize(h)
        self.nicewall.newSize(w, h)
        self.sitStillImaCloseIt.newSize((w, h))
        self.settings.newSize((w, h))
        self.help.newSize(h)

    """
        A block'o'code to select and eventually
        add/del images to the database
        and the program itself.
        Or to move them if they're weird
    """

    def refreshTags(self):
        for tag in self.pictureAddTab.textTags:
            self.currentTag = TagLabel(self, text=tag, showTag=self.addSearchTag)
            self.fileReader.createTag(tag, 0)
            self.tagsTab.listLayout.addWidget(self.currentTag)
        self.pictureAddTab.textTags = []
        self.pictureAddTab.fieldsClear()
        self.currentTag = None

    def closeAll(self):
        """
            Закрывает всё открытые табы.
        """
        self.sitStillImaCloseIt.close()
        self.nicewall.hide()
        self.pictureAddTab.shown = False
        self.pictureAddTab.setVisible(False)
        self.settings.shown = False
        self.settings.setVisible(False)
        self.tagsTab.shown = False
        self.tagsTab.setVisible(False)
        self.help.shown = False
        self.help.setVisible(False)
        if self.ui.searchLine.text() != "":
            self.ui.searchLine.setText("")
            self.search()
        # self.showPictures(request="")

    def showTagsWdgt(self):
        """
            Открывает / закрывает таб с тэгами.
        """
        if self.tagsTab.shown:
            self.closeAll()

        elif not self.tagsTab.shown:
            self.closeAll()
            self.sitStillImaCloseIt.open()
            self.tagsTab.shown = True
            self.tagsTab.setVisible(True)

    def showHelpWdgt(self):
        """
            Открывает / закрывает таб с тэгами.
        """
        if self.help.shown:
            self.closeAll()

        elif not self.help.shown:
            self.closeAll()
            self.sitStillImaCloseIt.open()
            self.help.shown = True
            self.help.setVisible(True)

    def addAcceptImage(self):
        """
            Добавление новых картинок / распаковка файла.
        """
        if self.pictureAddTab.drops.links:
            title = self.pictureAddTab.titleLine.text()
            file = self.pictureAddTab.drops.links.pop(0)
            index = file.rfind('.')

            if file[index:].lower() in ('.zip'):
                self.zipFolder = zipfile.ZipFile(file)
                for files in self.zipFolder.namelist():
                    if files.endswith('.jpg') or files.endswith('.jpeg') or files.endswith('.png'):
                        self.zipFolder.extract(files, self.fileReader._currentDir + "\\data")
                    else:
                        self.zipFolder.extract(files, self.fileReader._currentDir + "\\tempData")
                self.zipFolder.close()
                self.pictureAddTab.drops.refreshPicture()
                self.fileReader.combineData()
                self.showPictures()
            else:
                desc = self.pictureAddTab.desText.toPlainText()
                self.pictureAddTab.drops.refreshPicture()
                self.pictureAddTab.addTags()
                newPicture = self.fileReader.createPicture(file, 0, 0, desc)
                if newPicture:
                    if title:
                        oldpath = newPicture.path
                        path = oldpath[:oldpath.rfind('/')] + '/' + title + oldpath[oldpath.rfind('.'):]
                        try:
                            os.rename(oldpath, path)
                            self.fileReader.setPath(newPicture, path)
                            newPicture.path = path
                        except OSError:
                            print("OS ERRORR")
                    self.addImage(newPicture)

            self.rearrange()

    def addImage(self, pic: Picture, request=""):
        """
            Создание объекта мема и его добавление на основное окно.
        """
        file = pic.path
        if file:
            index = file.rfind('.')
            if (file[index:].lower() in SUPPORTED) and (index != -1):
                if not request:
                    request = 'all'
                if len(self.pictureAddTab.textTags) > 0:
                    # self.fileReader.addSomeTagsToPic(self.pictureAddTab.textTags, pic)

                    for tag in self.pictureAddTab.textTags:
                        self.fileReader.addTagToPic(self.fileReader.createTag(tag, 0, []), pic)
                self.labels[request].append(ImageLabel(parent=self.ui.memesLayout,
                                                       deletion=self.delImage,
                                                       pic=pic,
                                                       showTag=self.addSearchTag,
                                                       opening=self.openBigPic))
                self.labels[request][-1].setVisible(False)
                self.countUpdate()
                self.checkLines(self.ui.memesLayout.frameGeometry().width())
                self.addSpacer()
                self.s1.emit()
                # self.rearrange()
                self.show()

    def openBigPic(self, picture):
        """
            Открытие большого окна с картинкой при клике.

            picture = Picture, объект картинки.
        """
        self.closeAll()
        for item in self.labels['all']:
            if item.pic.path == picture.pic.path:
                pic = item
                break
        self.sitStillImaCloseIt.open()
        self.nicewall.show(pic)
        self.nicewall.setVisible(True)

    def delImage(self, label, forever=True):
        """
            Удаляет картинку. Навсегда. Прощай.

            label = ImageLabel, объект мема
            forever = bool, да.
        """
        if forever:
            self.fileReader.deletePicture(label.pic)
            for lbl in self.labels["all"]:
                if label.pic.path == lbl.pic.path:
                    self.delLbl(lbl)
        self.delLbl(label)
        self.rearrange()

    def delLbl(self, label):
        """
            Удаление упоминания объекта из памяти.

            label = ImageLabel, объект мема.
        """
        try:
            self.labels['all'].remove(label)
            if self.currReq != 'all':
                self.labels[self.currReq].remove(label)
        except ValueError:
            for i in self.labels["all"]:
                if i.pic.path == label.pic.path:
                    self.labels["all"].remove(i)
        label.deleteLater()

    def rearrange(self, request=""):
        """
            Супер хреновая пересортировка и отображение картинок.

            request = str, запрос, по которому отображаются картинки.
        """
        # Rearranging images in the grid

        for i in reversed(range(self.ui.gridLayout_3.count())):
            widgetToRemove = self.ui.gridLayout_3.itemAt(i)
            check = isinstance(widgetToRemove, QSpacerItem)
            if check:
                self.ui.gridLayout_3.removeItem(widgetToRemove)
                del widgetToRemove
            elif widgetToRemove:
                widgetToRemove = widgetToRemove.widget()
                self.ui.gridLayout_3.removeWidget(widgetToRemove)
                widgetToRemove.setParent(None)

        if request:
            if (request != self.currReq) and (self.currReq != 'all'):
                del self.labels[self.currReq]
                self.currReq = request

        sizes = []
        num = 0
        x = 0
        for i in range(len(self.labels[self.currReq])):
            if x >= self.memesInRow:
                sizes.append(num)
                num = 0
                x = 0
            if self.labels[self.currReq][i].imageV > num:
                num = self.labels[self.currReq][i].imageV
            x += 1

        if x != 0:
            sizes.append(num)

        x = 0
        y = 0
        for i in range(len(self.labels[self.currReq])):
            self.labels[self.currReq][i].setFixedHeight(sizes[y] + 50)
            x += 1
            if x >= self.memesInRow:
                x = 0
                y += 1

        x = 0
        y = 0
        for i in range(len(self.labels[self.currReq])):
            self.ui.gridLayout_3.addWidget(self.labels[self.currReq][i], x, y, Qt.AlignLeft | Qt.AlignTop)
            self.labels[self.currReq][i].gridPos = (x, y)
            self.labels[self.currReq][i].setVisible(True)
            y += 1
            if y >= self.memesInRow:
                y = 0
                x += 1

        self.countUpdate()
        self.stretchPics()
        self.addSpacer()

    """
        Block'o'code that works with positioning.
        i.e updating coordinates of images or
        aligning images to the top and lest

    """

    def countUpdate(self):
        """
            Уже не надо, в следующем пуше удалю.
        """
        # Based on the elements in a row slides to the new line

        self.count['x'] = len(self.labels[self.currReq]) // self.memesInRow

    def stretchPics(self):
        """
            Позволяет правильно отобразить картинки на поле по вертикале
        """

        for i in range(len(self.vspacers)):
            self.vspacers.pop(0)
        self.vspacers = []
        for i in range(self.memesInRow):
            self.vspacers.append(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))
            self.ui.gridLayout_3.addItem(self.vspacers[i], i, self.count['x'] + 1)

    def addSpacer(self):
        """
            Позволяет праильно отобразить картинки по горизонтали
        """
        for i in range(len(self.hspacers)):
            self.hspacers.pop(0)
        self.hspacers = []
        for i in range(self.count['x']):
            self.hspacers.append(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
            self.ui.gridLayout_3.addItem(self.hspacers[i], i, self.memesInRow + 2, 1, 1)

    def checkLines(self, x):
        """
            Проверка, сколько места есть под картинки.

            x = int, ширина окна
        """
        if self.labels[self.currReq]:
            amount = 1
            for i in range(len(PIXEL_LIST)):
                if PIXEL_LIST[i] < x:
                    amount = i + 2
                else:
                    break
            if amount != self.memesInRow:
                self.memesInRow = amount
                self.countUpdate()
                self.rearrange()
