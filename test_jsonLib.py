from shutil import rmtree
from jsonLib import Picture, FileReader, Tag
from os import rename, remove

fr = FileReader()


def test_first():
    try:
        remove(fr._currentDir + "\\data")
    except OSError:
        pass


def test_fileReader():
    assert fr == FileReader() == FileReader() == FileReader()


def comparePics(pic1: Picture, pic2: Picture):
    print(pic1.dictPic())
    print(pic2.dictPic())
    if pic1.ID == pic2.ID and pic1.path == pic2.path and pic1.width == pic2.width and \
       pic1.height == pic2.height and len(pic2.tagsID) == len(pic1.tagsID) and pic1.description == pic2.description:
        for i in range(len(pic1.tagsID) - 1):
            if pic1.tagsID[i] != pic2.tagsID[i]:
                return False
        return True
    else:
        return False


def compareTags(tag1: Tag, tag2: Tag):
    if tag1.name == tag2.name and tag1.color == tag2.color and len(tag1.synonyms) == len(tag2.synonyms):
        for i in range(len(tag1.synonyms)):
            if tag1.synonyms[i] != tag2.synonyms[i]:
                return False
        return True
    else:
        return False


def test_createPicture_simple():
    assert comparePics(fr.createPicture("anyPath/bla", 12, 21, "any string"), Picture(fr._currentDir + "/data/bla", 12,
                                                                                    21, 0, "any string", [], ""))


def test_loadNextPicture_simple():
    pic, ass = fr.loadNextPicture(True)
    assert ass == 0
    assert comparePics(pic, Picture(fr._currentDir + "/data/bla", 12, 21, 0, "any string", [], ""))


def test_createTag():
    assert compareTags(fr.createTag("name", hex(000), ["id", "qqq"]), Tag("name", hex(000), ["id", "qqq"]))


def test_loadNextTag():
    assert compareTags(fr.loadNextTag(True), Tag("name", hex(000), ["id", "qqq"]))


def test_addTagToPic():
    pic, ass = fr.loadNextPicture(True)
    tag = fr.loadNextTag(True)
    print(tag.dictTag())
    fr.addTagToPic(tag, pic)
    assert comparePics(fr.loadNextPicture(True)[0], Picture(fr._currentDir + "/data/bla", 12, 21, 0, "any string", ["name"], ""))


def test_deleteTagFromPic():
    fr.deleteTagFromPic(fr.loadNextTag(True), fr.loadNextPicture(True)[0])
    assert comparePics(fr.loadNextPicture(True)[0], Picture(fr._currentDir + "/data/bla", 12, 21, 0, "any string", [], ""))


def test_deleteTag():
    assert fr._tagData.get("name") is not None
    fr.deleteTag(Tag("name", hex(000), ["id", "qqq"]))
    assert fr._tagData.get("name") is None


def test_deletePicture():
    assert fr._data.get("0") is not None
    fr.deletePicture(Picture(fr._currentDir + "/bla", 12, 21, 0, "lbabla", [], ""))
    assert fr._data.get("0") is None


def test_last():
    try:
        remove(fr._currentDir + "/data")
    except OSError:
        pass